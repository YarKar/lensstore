﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LensStore.Models
{
    public class Product
    {
        public int ProductId { get; set; }                 

        public string Vendor { get; set; }

        public string Name { get; set; }

        public string Article { get; set; }

        public string Category { get; set; }

        public decimal Price { get; set; }
    }
}